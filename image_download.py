import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import requests
import matplotlib.pyplot as plt
import base64
from selenium.webdriver.chrome.service import Service
import os
import threading

def image_downloader(search):
    option=webdriver.ChromeOptions()
    option.headless=True

    url="https://www.google.com/imghp"
    service=Service(executable_path="./chromedriver.exe")
    driver = webdriver.Chrome(service=service,options=option)
    driver.get(url)
    input = driver.find_element(by="name", value='q')
    input.send_keys(search)
    input.send_keys(Keys.ENTER)
    # images_url = []
    image_block = driver.find_element(By.CLASS_NAME, value="mJxzWe")
    images_to_click = image_block.find_elements(By.CLASS_NAME, value="rg_i")
    dirpath = '_'.join(search.split())
    if not os.path.exists(f"./Celebrity Recognition Data/{dirpath}"):
        os.mkdir(f"./Celebrity Recognition Data/{dirpath}")
    img_num=1
    for image in images_to_click:
        image.click()
        time.sleep(3)
        image_links=driver.find_elements(By.CLASS_NAME, value="n3VNCb")
        for link in image_links:
            if 'http' in link.get_attribute('src'):
                # images_url.append(image.get_attribute('src'))
                img = requests.get(link.get_attribute('src'))
                File = open(f"./Celebrity Recognition Data/{dirpath}/{dirpath}_{img_num}.jpg", "wb")
                File.write(img.content)
                File.close()
                img_num += 1
                break
            # elif "base64" in image.get_attribute('src'):
            #     url=image.get_attribute('src').split(",")[1]
            #     img=base64.b64decode(url)
            #
            #     File = open(f"./Amber_heard/Amber_heard_{img_num}_b64.jpg", "wb")
            #     File.write(img)
            #     File.close()
            #     img_num += 1
            #     break
    driver.close()


# list_of_celeb=["Amber heard","scarlet johanson","Anjelina Joli","Shakira","Jenifer Lopez","Alia bhat"]

list_of_celeb=["Shakira","Dakota Johanson","Anne Hathway"]

if __name__=="__main__":
    for celeb in list_of_celeb:
        threading.Thread(target=image_downloader,args=(celeb,)).start()

